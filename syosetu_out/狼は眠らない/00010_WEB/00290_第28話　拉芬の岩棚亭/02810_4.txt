「我回來了。阿勒？有客人嗎？」
「妳好。我是阿利歐斯。這位叫作雷肯。今晚會在這裡留宿」
「啊啦啦。真有禮貌的人阿。也長得很帥呢」
「妮露。把食材拿到廚房吧」
「在生什麼氣阿。客人可在等著喔。馬上就會做美味的晚飯的」

納可和妻子妮露一起進了廚房，開始準備晚飯。
不久後做好了晚飯，端給了兩位住宿客。

「哎呀，這真好吃。在燉煮蔬菜前有炒過呢。香噴噴的。咬勁也不錯。話說回來，納可先生和妮露小姐待會也要吃飯嗎？」
「啊啊」
「可以的話要過來一起吃嗎？」

在迷惘著要怎麼回答時，妮露回答了。

「阿勒阿勒，真開心呢。竟然能被這種年輕好男人邀請阿」

然後，兩人的飯很快就被拿到了客人的桌上。

「有白酒嗎？」
「啊啊。有喔」
「那麼，能讓我請店主和夫人一杯白酒嗎？」
「阿呀，真是的。那我也請兩位各一杯白酒吧。還是說白酒不夠呢」
「怎麼會。那就不客氣地收下了」

四杯白酒很快就被準備好，四人乾了杯。
青年的話題很豐富，吃著飯聊著天，相當開心。
大男也是，雖然寡言，但也不會不親切，一起吃飯時不會讓人感到難受。
別說難受了，這頓飯非常愉快。納可和妮露被請了第二杯白酒。

途中，有客人來吃晚飯。這一天，除了住宿客、納可和妮露的飯以外有準備十五人分的飯，很快就賣完了。
吃完飯後，青年回房間了。說是累了想慢慢休息。
只是來吃飯的客人都回去後，大男仍在喝奇濁特燻酒。瓶子已經快空了。

「納可」

被大男叫了，納可便從櫃台另一邊看向大男。

「如果不戰鬥直接跑下去，要多久才能抵達最下層？」
「你說什麼」
「不戰鬥直接下階梯，從第一階層到最下層要花多久，是這麼問的」

這傢伙在說什麼鬼話，納可想著。

此時想到了。
在別的迷宮，說不定有能不跟魔獸戰鬥直接下階梯的場合。

「這裡的迷宮的階梯阿。是在大型個體的房間裡」
「房間？」
「對。這裡的迷宮叫岩穴型，是有通路和房間的構造。房間裡有魔獸。也有沒有魔獸的房間。不進房間就不用跟魔獸戰鬥。但是，往下的階梯在大型個體的房間裡，所以不戰鬥就沒辦法下去。不，嘛，雖然不打倒也能通過，但這種做法很快就會陷入瓶頸。而且到了中層的話，實際的問題是，沒辦法不戰鬥就闖進階梯」
「喔？為什麼」
「去了就知道了」
「淺階層原本是打算不戰鬥直接通過的，看來沒辦法這樣嗎」
「這裡跟別的地方的迷宮不同，沒有面向初心者的敵人。只能一邊以戰鬥提升實力，一邊慢慢地前進」
「是嗎。話說回來，出現大型個體的房間是固定的嗎」
「那當然了」
「一個階層裡，大型個體的房間，也就是有階梯的房間，會有幾個」
「兩個，吧？我也不知道詳細情況。不過，大概，大多階層都是兩個」
「呼嗯。這種構造的話，要從下方階層上去上方階層時，也得跟大型個體戰鬥阿」
「上去？你說上去迷宮？為什麼要做這種事？」
「不，也沒打算要做。只是問問而已。是嗎。有迴廊，房間裡有魔獸，然後有頭目房嗎」
「啊啊，頭目房嗎。是這意思阿。原來如此，這麼說來也是。總之這裡的迷宮的構造似乎挺稀奇的。第一次的話可能會有疑惑吧」
「不。對我來說是很熟悉的構造」

大男臉上浮出了猙獰的笑容。

「既然要僱用鼠，那就問鼠就好。會直接帶去有大型個體的房間。但是實際的探索，果然還是跟那階層的普通魔獸戰鬥幾次，把握特徵後再挑戰大型個體會比較好」
「喔。各階層會有不同特質嗎？」
「啊啊。魔法的耐性，跟你無關吧。動作速度，整體有多難纏，各階層都不同。直接跟大型個體戰鬥會反應不了吧」
「原來如此」
「總之加油吧。要注意恩寵品魔獸喔」
「恩寵品魔獸是什麼」
「連這也不知道嗎。在這迷宮，如果魔獸持有的武器有恩寵，打倒的時候經常會出現放了那恩寵武器的寶箱。應該說，如果那真的是恩寵品的話，據說一定會掉落那恩寵品」
「什麼」
「喂喂。真起勁阿。總之，持有恩寵品的魔獸，難纏度差很多。在這迷宮死的人，大多是被恩寵殺的。畢竟淺階層也會出現持有恩寵品的魔獸。所以，這裡對新手來說太困難了」
「呼呼。是嗎，是嗎。打倒了拿著附恩寵的武器的魔獸，就能得到那恩寵武器嗎。是嗎是嗎」
「各階層容易出現什麼種類的恩寵品，就去問鼠吧。不過，出現的恩寵品有很多種類。以為會出現會爆發的劍，結果出現的是被傷到會降低速度的劍」
「呼嗯。真有趣。最下層的頭目房，不對，出現大型個體的房間，有限制進入人數嗎？」
「人數限制？不，沒聽過這種事。不論是最下層還是別的階層，應該都不會不能進入幾人以上。不過，房間的大小是固定的。要是太多人就很難行動」

大男把最後的酒喝光後，把杯子放在桌上。

「聽了好東西了。這是謝禮」
「喂喂，這不是金幣嗎。只是說了大家都知道的事而已，可不能收下這種東西」
「對我來說是有這等價值的情報。收下吧」

猶豫了一下後，納可收下了那金幣。
不論如何，都不能只是聊個天就收下金幣。時不時給點下酒菜吧，納可想著。