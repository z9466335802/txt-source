２５

「那麼，就在這裡道別吧。」

接下來，雷肯等人要向北走。
而赫蕾絲，要向南。

向南行一段距離，有個叫畢格的城鎮，那裡的領主的兒子似乎是知己的樣子。說是要請畢格的領主安排，來把自己和女王種的頭與腳，送到王都去。

「雷肯殿。」

赫蕾絲回過了頭。
留下了些許激戰之痕跡的那鎧姿，非常美麗。

聖硬銀之劍在剝取完女王種後，就很乾脆地還回來了，現在收在腰上的，是從王都帶來的劍。
那估計是出於名工之手的，高級而細緻的結構，看一眼就知道很有價值。
原來如此，再次認知到，在迷宮外看到的話，不管是誰都會知道，是高位貴族家的女性。

赫蕾絲走向雷肯，輕輕伸出了右手手掌。
手掌怯生生地舉高，輕輕地放在了，雷肯的左胸上。
宛如要體會那溫暖。

然後赫蕾絲以左手，包覆了自己的左胸。
連接著心臟與心臟，這動作究竟有什麼意義，雷肯並不知道。但是，抬起來筆直地看著雷肯的臉的，赫蕾絲的瞳孔，是從未見過之清澈，正溫柔地凝視著。

阿利歐斯和艾達都不發一語，看著別離的儀式。
這相差甚遠的，野獸般的男人與高貴的女騎士在人生交錯之偶然，恐怕不會再有第二次了吧。
經過了很長的時間後，赫蕾絲收回了手，繼續看著雷肯，像是回想起來似地說道。

「話說回來，那自我中心的男人，說不定會打擾雷肯殿。」
「誰？」
「斯諾。」
「啊啊，那個嗎。」
「那男人自報是，普萊耶的派澤倫家的人。會透過父親大人來安排，處理好派澤倫家，讓那男人沒辦法對您造成麻煩。」
「這樣阿。幫大忙了。」

赫蕾絲將右膝跪到地上，右手在胸，左手碰劍，行了禮。
然後站了起來，說出了道別的話語。

「再會了。雷肯殿。」
「要保重喔。赫蕾絲小姐。」
「謝謝。艾達殿。阿利歐斯殿也是。與您們度過的日子，是我一生的寶物。」
「赫蕾絲小姐。等回去之後，請幫我向您父親問好。」
「誒？啊啊。」
「帶了這麼厲害的伴手禮回去，大家都會嚇一大跳吧。」
「是阿。嗚呼呼呼。只要一想像叔父大人看到這些時的表情。庫、庫、庫、庫。」
「叔叔？」
「是的。叔父大人他，大大反對我作為騎士服侍尊貴之人士。主張說，會介紹結婚對象所以給我結婚，那就是你的幸福什麼的。」
「這，這樣阿。」
「還說，如果不論如何都要服侍的話，就不要作為騎士而是作為侍女來服侍，然後濫用地位，提出了這次這不合理的條件。而且因為叔父大人事先找了王都和有力都市的冒険者協會安排，所以沒辦法僱用高位的冒険者。」
「還有那種事阿。」
「但是我讓奇蹟成真了。托雷肯殿和您們的福。叔父大人決定的條件會束縛住他自己。什麼都反駁不了。哈哈哈哈哈。」

赫蕾絲意氣洋洋地離去了。
三人目送那背影消失於山丘的另一端。

「雷肯殿。」
「什麼。」
「阿，稱呼變回〈殿〉了。」
「說不定我們。」
「嗯？」
「是不是關照過頭了。」
「誰知道。」


２６

〈尖岩〉給的袋子，其中一個放了六枚白金幣，另一個放了首飾。

是魔法使貝塔戴的首飾。
有著怎樣的恩寵呢，之後的鑑定真讓人期待。

（總算能回去了）

雷肯喜歡迷宮探索。
喜歡邂逅強敵，喜歡和強敵戰鬥，喜歡戰勝強敵。然後，會為在戰鬥之中自己變得更強一事，感到無上之喜悅。

儘管如此，這次的迷宮探索，是有點累了。
發生太多事情了。
首先，想好好睡一覺。
想回家靜下來，悠哉生活一陣子。

想到這，突然注意到了。

（能回去了？）
（家？）

至今以來，雷肯也有在短時間擁有屋子居住過。
也有以城鎮為根據地活動過一陣子。
但是，那只是個據點，是隨時都能替換的，何時失去都無所謂之物。

沃卡又如何呢。
那城鎮，不壊。

領主雖然有些乖僻，但是個有能力與信念的男人，城裡的人，說是過得還算幸福也行。雖然也有窮人和感到不講理的人，但應該說不上有被欺凌過頭。
神殿也是，雖然有腐敗的部分，但也多少有在活動。
冒険者協會，還挺行的。職員的資質也不錯。
傑尼。
諾瑪。
因為委託而遇到的人們。
然後是，希拉。

對。那城鎮，不壊。
所以走向那城鎮時，才會如此輕快。

雷肯對於在兩個世界中，自己第一次有了像是故鄉的東西，儘管感到困惑，但也將其接納了。

「阿。能看到沃卡城了！我們，回來了喔！」
「是阿。回來了呢。」